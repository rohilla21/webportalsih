## Problem
Presently, film makers have been approaching the concerned agencies independently or in some cases through DTTDC, for the purpose. DTTDC operates by forwarding and following, their requests with concerned 16 stakeholder agencies, having nearly 200 film shooting locations, which process the same internally by receiving offline applications as per their prescribed formats and charge pre-defined fee to grant permissions for shooting films in their domain areas. These stakeholders agencies belong to the different departments related to Government of India, Government of Delhi, Municipal Bodies and Police etc. These departments are Not linked online and all communication happens in offline mode.

## Solution
An online web portal linked with all stakeholder agencies is needed for creating a Single Window Clearance system, which may facilitate to receive online application, to be processed by concerned agencies and grant online approvals for film shooting besides accepting requisite fees through e-commerce transactions. Proposed online solution may provide hassle free and transparent process.



## Key Features
* Registration and authentication (using Social Media Platforms)
* Application Status Information (eg. Approve, Reject or Modifiy)
* Booked Dates shown in red color in calender to producer while booking any location
* Stakeholder Dashboard to view all applications
* Applicant Dashboard to view his applications & make any updates
* Admin Dashboard to view all stakeholders & producer related application information
* Stakeholder Alert on New Application & Change of status using Email(sendgrid) & SMS (msg91)
* SMS & Email Alerts on status update to authorities, Admin User & Producer 
* 24hr window ( If the stakeholder does not respond within 24hr an email & SMS reminder and a report will be filed to the     department head )
* New Location Request by Producers
* Application Status Tracking
* Payment API Integration (Razorpay) 
* Application form filling for multiple locations

#### Future Aspects
* Foreign Producer Application  
* Automatic Location grant & dynamic Pricing using Artificial Intelligence & Big-data  
* Extending the application to manage locations all over India under on unified window for all states


## Implementation
#### Dependencies
Python 3.5.2  

Django 2.0  

Social-auth-app-django 2.1.0
    
 
#### Installation  
Clone master and activate the virtualenv using `source venv/bin/activate.  

Run requirements.txt  
`pip3 install -r requirements.txt`  

`python3 manage.py makemigrations`

`python3 manage.py migrate`

`python3 manage.py runserver`
