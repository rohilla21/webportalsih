#include <GL/glut.h>  
#include <iostream>
#include <cmath>
using namespace std;

int sx,sy,sx1,sy1,te,clr=1;
int ex,ey,c=0,scale,angle;
int r=0;
int w,h,k=0,nx,ny,ox,oy,a,b;
int arr[500][2];


void display() {
  
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);  

   glBegin(GL_POINTS);              
    for(int i=-w/2; i<=w/2; i++){
      glVertex2i(i,0);
    }

    for(int i=-h/2; i<=h/2; i++){
      glVertex2i(0,i);
    }
   glEnd();
   glFlush(); 
}
void plotpoint(GLint x,GLint y){
  glBegin(GL_POINTS);
  
  glVertex2i(x,y);
  glEnd();
 
}
void drag(){
  glColor3f(0.0,0.0,0.0);
  glBegin(GL_POINTS);
  for(int i=0;i<k;i++){
    if(arr[i][0]!=0&&arr[i][1]!=0)
     glVertex2i(arr[i][0],arr[i][1]);
  }
  glColor3f(1.0,1.0,1.0);
  for(int i=0;i<k;i++){
     glVertex2i(arr[i][0]-ox+nx,arr[i][1]-oy+ny);
  }
  glEnd();
  glFlush();
}


struct Point {
	GLint x;
	GLint y;
};

struct Color {
	GLfloat r;
	GLfloat g;
	GLfloat b;
};

Color newColor = {1.0f, 0.0f, 1.0f};
Color oldColor = {1.0f, 1.0f, 1.0f};


Color getPixelColor(GLint x, GLint y) {
	Color color;
	glReadPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, &color);
	return color;
}

void setPixelColor(GLint x, GLint y, Color color) {
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
	glFlush();
}

void floodFill(GLint x, GLint y, Color oldColor, Color newColor) {
	Color color;
	color = getPixelColor(x, y);

	if(color.r == oldColor.r && color.g == oldColor.g && color.b == oldColor.b)
	{
		setPixelColor(x, y, newColor);
		floodFill(x+1, y, oldColor, newColor);
		floodFill(x, y+1, oldColor, newColor);
		floodFill(x-1, y, oldColor, newColor);
		floodFill(x, y-1, oldColor, newColor);
	}
	return;
}

void plotellipse(){
  int x1=0;
  int x=sx;
  int y=sy;
  int y1=b;
int p=4*b*b+a*a-4*a*a*b;
 arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  
 arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(x1+x,-y1+y);

   glEnd();
  
  
  while((b*b)*x1<=(a*a)*y1){
    if(p<0){
      x1++;
      p=p+b*b+2*x1*b*b;
      
    }
    else{
      x1++;
      y1--;
      p=p+b*b+2*b*b*x1-2*a*a*y1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(100.0,100.0,1.0);
   glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
  x1=a;
  y1=0;
 p=b*b+4*a*a-4*b*a*b;
  arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(-x1+x,y1+y);
 
  cout<<a<<" "<<b<<" "<<endl;
  
  while((b*b)*x1>(a*a)*y1){
    if(p<0){
      y1++;
      p=p+a*a+2*y1*a*a;
      
    }
    else{
      x1--;
      y1++;
      p=p+a*a+2*a*a*y1-2*b*b*x1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(100.0,100.0,1.0);


       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
 glFlush();
  
  
}

void plotellipse2(){
  int x1=0;
  int x=sx;
  int y=sy;
  int y1=b;
int p=4*b*b+a*a-4*a*a*b;
 arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  
 arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(x1+x,-y1+y);

   glEnd();
  
  
  while((b*b)*x1<=(a*a)*y1){
    if(p<0){
      x1++;
      p=p+b*b+2*x1*b*b;
      
    }
    else{
      x1++;
      y1--;
      p=p+b*b+2*b*b*x1-2*a*a*y1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);
glColor3f(0.0,1.0,250.0);
  glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
  x1=a;
  y1=0;
 p=b*b+4*a*a-4*b*a*b;
  arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(-x1+x,y1+y);
 
  cout<<a<<" "<<b<<" "<<endl;
  
  while((b*b)*x1>(a*a)*y1){
    if(p<0){
      y1++;
      p=p+a*a+2*y1*a*a;
      
    }
    else{
      x1--;
      y1++;
      p=p+a*a+2*a*a*y1-2*b*b*x1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(0.0,1.0,250.0);

       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
 glFlush();
  
  
}

void plotellipse3(){
  int x1=0;
  int x=sx;
  int y=sy;
  int y1=b;
int p=4*b*b+a*a-4*a*a*b;
 arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  
 arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(x1+x,-y1+y);

   glEnd();
  
  
  while((b*b)*x1<=(a*a)*y1){
    if(p<0){
      x1++;
      p=p+b*b+2*x1*b*b;
      
    }
    else{
      x1++;
      y1--;
      p=p+b*b+2*b*b*x1-2*a*a*y1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(0.0,255.0,20.0);
       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
  x1=a;
  y1=0;
 p=b*b+4*a*a-4*b*a*b;
  arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(-x1+x,y1+y);
 
  cout<<a<<" "<<b<<" "<<endl;
  
  while((b*b)*x1>(a*a)*y1){
    if(p<0){
      y1++;
      p=p+a*a+2*y1*a*a;
      
    }
    else{
      x1--;
      y1++;
      p=p+a*a+2*a*a*y1-2*b*b*x1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(0.0,255.0,20.0);


       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
 glFlush();
  
  
}

void plotellipse4(){
  int x1=0;
  int x=sx;
  int y=sy;
  int y1=b;
int p=4*b*b+a*a-4*a*a*b;
 arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  
 arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(x1+x,-y1+y);

   glEnd();
  
  
  while((b*b)*x1<=(a*a)*y1){
    if(p<0){
      x1++;
      p=p+b*b+2*x1*b*b;
      
    }
    else{
      x1++;
      y1--;
      p=p+b*b+2*b*b*x1-2*a*a*y1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(100.0,0.0,100.0);

       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
  x1=a;
  y1=0;
 p=b*b+4*a*a-4*b*a*b;
  arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(-x1+x,y1+y);
 
  cout<<a<<" "<<b<<" "<<endl;
  
  while((b*b)*x1>(a*a)*y1){
    if(p<0){
      y1++;
      p=p+a*a+2*y1*a*a;
      
    }
    else{
      x1--;
      y1++;
      p=p+a*a+2*a*a*y1-2*b*b*x1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(100.0,0.0,100.0);

       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
 glFlush();
  
  
}

void plotellipse5(){
  int x1=0;
  int x=sx;
  int y=sy;
  int y1=b;
int p=4*b*b+a*a-4*a*a*b;
 arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  
 arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(x1+x,-y1+y);

   glEnd();
  
  
  while((b*b)*x1<=(a*a)*y1){
    if(p<0){
      x1++;
      p=p+b*b+2*x1*b*b;
      
    }
    else{
      x1++;
      y1--;
      p=p+b*b+2*b*b*x1-2*a*a*y1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(0.0,100.0,1.0);

       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
  x1=a;
  y1=0;
 p=b*b+4*a*a-4*b*a*b;
  arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  glBegin(GL_POINTS);
  glVertex2i(x1+x,y1+y);
     glVertex2i(-x1+x,y1+y);
 
  cout<<a<<" "<<b<<" "<<endl;
  
  while((b*b)*x1>(a*a)*y1){
    if(p<0){
      y1++;
      p=p+a*a+2*y1*a*a;
      
    }
    else{
      x1--;
      y1++;
      p=p+a*a+2*a*a*y1-2*b*b*x1;
    }
     arr[k][0]=x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=x1+x;
  arr[k][1]=-y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=y1+y;
  k++;
  arr[k][0]=-x1+x;
  arr[k][1]=-y1+y;
  k++;
     cout<<x1<<" "<<y1<<endl;
    glBegin(GL_POINTS);

glColor3f(0.0,100.0,1.0);


       glVertex2i(x1+x,y1+y);

     glVertex2i(x1+x,-y1+y);
       glVertex2i(-x1+x,y1+y);
  
     glVertex2i(-x1+x,-y1+y);
       glEnd();
  }
 glFlush();
  
  
}

void rotate(){
  float ang=(float)(3.14/180)*angle;
  glColor3f(0.0,0.0,1.0);
  for(int i=0;i<k;i++){
   // cout<<"h"<<i<<" ";
     glBegin(GL_POINTS);
    glVertex2i((arr[i][0]-sx)*cos(ang)-(arr[i][1]-sy)*sin(ang)+sx,(arr[i][0]-sx)*sin(ang)+(arr[i][1]-sy)*cos(ang)+sy);
    glEnd();
    glFlush();
  }
}
void scale_fun(){
  for(int i=0;i<k;i++){
    glBegin(GL_POINTS);
    glVertex2i((arr[i][0]-sx)*scale+sx,(arr[i][1]-sy)*scale+sy);
    glEnd();
    glFlush();
  }
}
 void mousePtPlot (GLint button, GLint action, GLint xMouse, GLint yMouse)
{
    if (button == GLUT_LEFT_BUTTON && action == GLUT_DOWN)
    { 
       if(c==0){
          sx=xMouse-w/2;
          sy=h/2-yMouse;
          cout<<"Center: ("<<sx-w/2<<","<<h/2-sy<<")\n";
           c++;
         }
         else if(c==1) {
		sx1=sx;
	sy1=sy1;
            ex=sx1+5;
            ey=sy1;
            cout<<"other point: ("<<ex-w/2<<","<<h/2-ey<<")\n";
            
            r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);

             plotellipse();
 floodFill(sx, (sy+1), oldColor, newColor);
	

            c++;
	
           }
	/*for(te=0;te<4;te++){
	
	ex+=5;
            ey+=5;
	 r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);
             plotellipse();

	}*/
ex+=5;
            ey+=5;
	 r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);
             plotellipse2();

ex+=5;
            ey+=5;
	 r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);
             plotellipse3();

ex+=5;
            ey+=5;
	 r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);
             plotellipse4();
ex+=5;
            ey+=5;
	 r=sqrt((ex-sx)*(ex-sx)+(ey-sy)*(ey-sy));
           b=abs(ey-sy);
           a=abs(ex-sx);
             plotellipse5();

}
if (button == GLUT_RIGHT_BUTTON && action == GLUT_DOWN)  
{cout<<"Enter the value of angle: ";
cin>>angle;
            // c=0;
              rotate();

}
      
    
    glFlush();
   
    
}
void key(unsigned char key_t, GLint x, GLint y)
{
 if(key_t=='s')
  {
cout<<"Enter the value of scaling: ";
              cin>>scale;
              c=0;
              scale_fun();
  }

if(key_t=='d')
{  
	 nx=x-w/2;
            ny=h/2-y;
            drag();
	
}
}
void init ()
{
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glMatrixMode (GL_PROJECTION);
   // cout<<w<<" "<<h<<endl;
    glOrtho(-w/2 ,w/2, -h/2, h/2, -1.0, 1.0);
   
}
int main(int argc, char** argv) {

   glutInit(&argc, argv); 
   glutInitWindowPosition(0, 0);
       glutInitWindowSize (500, 500);
   glutCreateWindow("Trans + Scale + Rotation"); 
   w=glutGet(GLUT_WINDOW_WIDTH);
   h=glutGet(GLUT_WINDOW_HEIGHT);
   init();

  glutDisplayFunc(display); 
   glFlush();
    glutMouseFunc (mousePtPlot);
glutKeyboardFunc(key); 
   
   glutMainLoop();         
   return 0;
}
