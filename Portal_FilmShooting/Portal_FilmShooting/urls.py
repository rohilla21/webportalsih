from django.contrib import admin
from django.urls import include, path, re_path

urlpatterns = [
    re_path('^', include(('accounts.urls', 'accounts'), namespace='accounts')),
    path('admin/', admin.site.urls),
    path('social-auth/', include('social_django.urls', namespace='social')),
    path('booking/', include(('booking_places.urls', 'booking_places'), namespace='booking_places')),
]
