from django.apps import AppConfig


class BookingPlacesConfig(AppConfig):
    name = 'booking_places'
