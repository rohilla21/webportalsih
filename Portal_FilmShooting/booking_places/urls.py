from . import views
from django.conf import settings
from django.urls import path, re_path


urlpatterns = [
	re_path(r'^$', views.home, name='home'),
	re_path(r'^submit_application_form/', views.application_form, name='application_form'),
	# re_path(r'^check_application/$', views.check_application, name='check_application'),
	re_path(r'^select_locations/$', views.render_select_locations, name='select_locations'),
	re_path(r'^check_dates/$', views.check_dates, name='check_dates'),
	re_path(r'^submit_application_location_form/', views.application_location_form, name='application_location_form'),
	re_path(r'^payment_result/', views.payment_result, name='payment_result'),
	re_path(r'^dashboard/', views.dashboard, name='dashboard'),
	re_path(r'^check_application/(?P<application_id>\d+)/$', views.check_application, name='check_application'),
]
