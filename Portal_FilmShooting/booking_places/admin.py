from django.contrib import admin
from .models import Application, Location, ApplicationLocation
from .forms import LocationForm
from social.apps.django_app.default.models import Association, Nonce, UserSocialAuth
from django.contrib.admin import SimpleListFilter


class ApplicationLocationAdmin(admin.ModelAdmin):
	list_display = ['application','current_status','shoot_start_date','shoot_end_date', 'remarks']
	list_filter=['shoot_start_date','shoot_end_date', 'time_of_modification']
	list_per_page=8

	def get_queryset(self, request):
		qs = super(ApplicationLocationAdmin, self).get_queryset(request)
		if request.user.is_superuser:
			return qs
		locations = Location.objects.filter(stakeholder=request.user)
		return qs.filter(location__in=locations)


class ApplicationAdmin(admin.ModelAdmin):
	list_display=['producer','film','production_house','production_house_address','contact','local_line_producer','local_line_producer_address','director','cinematographer','art_director','script','undertaking']
	list_per_page=8
	def get_queryset(self, request):
		qs = super(ApplicationAdmin, self).get_queryset(request)
		if request.user.is_superuser:
			return qs
		locations = Location.objects.filter(stakeholder=request.user)
		return qs.filter(applicationlocation__location__in=locations)


class LocationAdmin(admin.ModelAdmin):
	form = LocationForm
	list_display = ['name', 'price','stakeholder']
	list_filter=['price']
	list_per_page=8


	def save_model(self, request, location, form, change):
		location.stakeholder = request.user
		location.save()


admin.site.register(ApplicationLocation, ApplicationLocationAdmin)
admin.site.register(Application, ApplicationAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.unregister(Association)
admin.site.unregister(Nonce)
admin.site.unregister(UserSocialAuth)
