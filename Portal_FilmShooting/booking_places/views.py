from django.shortcuts import render,redirect, HttpResponse
from .models import Application, Location, ApplicationLocation, Payment
from datetime import datetime, timedelta
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from dateutil.parser import parse
from urllib import parse as urlparse
from common.utils import send_email, send_sms
from django.contrib.auth.decorators import login_required
# Create your views here.
def home(request):
    return render(request, 'application_form.html')


@csrf_exempt
def application_form(request):
    if request.method != 'POST':
        return

    script = request.FILES['script']
    undertaking = request.FILES['undertaking']
    data = request.POST

    film = data.get('film')
    production_house = data.get('production_house')
    production_house_address = data.get('production_house_address')
    contact = data.get('contact')
    local_line_producer = data.get('local_line_producer')
    local_line_producer_address = data.get('local_line_producer_address')
    director = data.get('director')
    cinematographer = data.get('cinematographer')
    art_director = data.get('art_director')
    script = data.get('script')
    undertaking = data.get('undertaking')

    application = Application(producer=request.user,
                              film=film,
                              production_house=production_house,
                              production_house_address=production_house_address,
                              contact=contact,
                              local_line_producer=local_line_producer,
                              local_line_producer_address=local_line_producer_address,
                              director=director,
                              cinematographer=cinematographer,
                              art_director=art_director,
                              script=script,
                              undertaking=undertaking)
    application.save()
    request.session['current_application_id'] = application.pk
    return redirect('/booking/select_locations')


def render_select_locations(request):
    locations = Location.objects.all()
    return render(request, 'select_locations.html', {
        'locations': locations,
    })


def grouped(iterable, n):
    return zip(*[iter(iterable)]*n)


@csrf_exempt
def application_location_form(request):
    if request.method != 'POST':
        return

    data = request.body
    locations = json.loads(data)
    for x, y in grouped(locations, 2):
        location_id = x.get('value')
        # 03/31/2018 - 03/31/2018
        datetimepicker = y.get('value')
        start_date = datetimepicker.split(' - ')[0]
        end_date = datetimepicker.split(' - ')[1]
        start_date = parse(start_date)
        end_date = parse(end_date)
        application_id = request.session['current_application_id']
        application_location = ApplicationLocation(location_id=location_id,
                                                   application_id=application_id,
                                                   shoot_start_date=start_date,
                                                   shoot_end_date=end_date)
        application_location.save()
    return HttpResponse()


@csrf_exempt
def payment_result(request):
    payment_id = request.POST.get('payment_id')
    Payment.objects.create(payment_id=payment_id)
    user = request.user
    send_email(user.email, "Delhi toursim email", 'Hi, your payment is successfull.')
    send_sms(8872952689, 'Hi {}, Your payment is successful - Delhi Tourism'.format(user.first_name))
    send_sms(9888470272, 'Delhi Tourism: You have received a new application')
    return redirect('/booking/dashboard')


def dashboard(request):
    STATUS_CHOICES = (
         (1, 'Pending'),
         (2, 'Payment Required'),
         (3, 'Approved'),
         (4, 'Application Rejected'),
         (5, 'Modify'),
         (6, 'No Payment'),
        )
    producer = request.user
    applications = Application.objects.filter(producer=producer)
    data = []

    index = 1
    for application in applications:
        item = {}
        application_locations = ApplicationLocation.objects.filter(application=application)
        item['application_locations'] = application_locations
        item['application'] = application
        current_status = application_locations[0].current_status
        current_status_info = STATUS_CHOICES[current_status][1]
        item['current_status_info'] = current_status_info
        item['index'] = index
        index = index + 1
        data.append(item)

    return render(request, 'dashboard.html', {'data': data})


def check_application(request,application_id):
    application = Application.objects.get(pk=int(application_id))
    return render(request,'check_application.html',{'application':application})


@csrf_exempt
def check_dates(request):
    if request.method != 'POST':
        return

    import json
    data = json.loads(request.body)
    new_shoot_start_date = data.get('start_date')
    new_shoot_end_date = data.get('end_date')
    if not new_shoot_start_date or not new_shoot_end_date:
        return JsonResponse({'message': 'Empty post data', 'invalid':True})

    # accepted_form = ApplicationLocation.objects.filter(
    #     Q(location_id=location_id), Q(current_status=2) | Q(current_status=3),
    #     Q(time_of_modification__gte=)
    # )
    # old_shoot_start_date = accepted_form.shoot_start_date
    # old_shoot_end_date = accepted_form.shoot_end_date
    #
    #     if(new_shoot_end_date.time() <old_shoot_end_date.time() and new_shoot_end_date.time()>old_shoot_start_date.time()):
    #         message='Shoot dates are clashing, Modify your application. For more information contact the authorities'
    #         return JsonResponse({'message':message,'invalid':True})
    #     elif(new_shoot_start_date.time()>old_shoot_start_date.time() and new_shoot_end_date.time()<old_shoot_end_date.time()):
    #         message='Shoot dates are clashing, Modify your application. For more information contact the authorities'
    #         return JsonResponse({'message':message,'invalid':True})
    #     elif(new_shoot_start_date.time()>old_shoot_start_date.time() and new_shoot_end_date.time()<old_shoot_end_date.time()):
    #         message='Shoot dates are clashing, Modify your application. For more information contact the authorities'
    #         return JsonResponse({'message':message,'invalid':True})
    #     else:
    #      message='&#10004'
    #      return JsonResponse({'message':message,'invalid':True})
    #  except:
    #      message='&#10004'
         # return JsonResponse({'message':message,'invalid':False})
    return JsonResponse({'message':'', 'invalid': False})
