from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User


class Location(models.Model):
    name = models.CharField(max_length=100, default='')
    price = models.IntegerField()
    stakeholder = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Application(models.Model):
    producer = models.ForeignKey(User, on_delete=models.CASCADE)
    film = models.CharField(max_length=100)
    production_house = models.CharField(max_length=100, default='')
    production_house_address = models.CharField(max_length=300, default='')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact = models.CharField(validators=[phone_regex], max_length=17,default='')
    local_line_producer = models.CharField(max_length=100, blank=True, default='')
    local_line_producer_address = models.CharField(max_length=300, blank=True, default='')
    director = models.CharField(max_length=100)
    cinematographer = models.CharField(max_length=100)
    art_director = models.CharField(max_length=100)
    script = models.FileField(upload_to='scripts/')
    undertaking = models.FileField(upload_to='undertaking/', default='')
    time_of_modification = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.film


class ApplicationLocation(models.Model):
    STATUS_CHOICES = (
         (1, 'pending'),
         (2, 'paymentRequired'),
         (3, 'approved'),
         (4, 'applicationRejected'),
         (5, 'modify'),
         (6, 'noPayment'),
        )

    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    current_status = models.SmallIntegerField(choices=STATUS_CHOICES, default=1)
    shoot_start_date = models.DateField()
    shoot_end_date = models.DateField()
    remarks = models.CharField(max_length=200, null=True, blank=True)
    time_of_modification = models.DateTimeField(auto_now=True)
    time_of_creation = models.DateTimeField(auto_now_add=True)


class Payment(models.Model):
    payment_id = models.CharField(max_length=100)
