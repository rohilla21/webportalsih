from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class Profile(models.Model):
	ACCESS_OPTIONS = (
		('Admin', 'Admin'),
		('Client', 'Client'),
		('Customer', 'Customer'),
		)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	access = models.CharField(max_length=10, choices=ACCESS_OPTIONS, null=False)
	
	def __str__(self):
		return self.user.username


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.user.username

def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)