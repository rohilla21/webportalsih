from . import views
from django.conf import settings
from django.urls import path, re_path


urlpatterns = [
	re_path(r'^$', views.index, name='index'),
	re_path(r'^index/$', views.index, name='index'),
	re_path(r'^login/$', views.login_view, name='login_view'),
	re_path(r'^logout/$', views.logout_view, name='logout_view'),
	re_path(r'^register/$', views.register, name='register'),
	re_path(r'^profiledpdn/$', views.profile_view, name='profile_view'),
	re_path(r'^profile/$', views.view_profile, name='view_profile'),
    re_path(r'^profile/(?P<pk>\d+)/$', views.view_profile, name='view_profile_with_pk'),
    re_path(r'^profile/edit/$', views.edit_profile, name='edit_profile'),
    re_path(r'^change-password/$', views.change_password, name='change_password'),
	
]